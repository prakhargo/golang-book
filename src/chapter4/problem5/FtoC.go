//
// Copyright (c) 2016 by Prakhar Goyal. All Rights Reserved.
//

package main

import "fmt"

func main() {

	// Write a program that converts from Fahrenheit into Celsius. (C = (F - 32) * 5/9)

	fmt.Print("Enter temperature in fahrenheit: ")
	var fahrenheit float64
	fmt.Scanf("%f", &fahrenheit)
	celsius := (fahrenheit - 32) * 5 / 9
	fmt.Println("Temperature in celsius:", celsius)
}
