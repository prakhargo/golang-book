//
// Copyright (c) 2016 by Prakhar Goyal. All Rights Reserved.
//

package main

import "fmt"

func main() {

	// Write a program that converts from feet into meters. (1 ft = 0.3048 m)

	fmt.Print("Enter distance in feet: ")
	var feet float64
	fmt.Scanf("%f", &feet)
	metre := feet * 0.3048
	fmt.Println("Temperature in metre:", metre)
}
