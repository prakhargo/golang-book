//
// Copyright (c) 2016 by Prakhar Goyal. All Rights Reserved.
//

package main

import "fmt"

func main() {
	fmt.Println("1 + 1 =", 1.0+1.0)	//Println separates the entities by a space.
	fmt.Println(3/2.0)

	fmt.Println(len("Hello World"))
	fmt.Println("Hello World"[1])
	fmt.Println("Hello " + "World")
	//fmt.Println("Hello" + 1) 	//Error: mismatched types

	fmt.Println(321325 * 424521)
	fmt.Println((true && false) || (false && true) || !(false && false))
}
