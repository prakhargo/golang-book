//
// Copyright (c) 2016 by Prakhar Goyal. All Rights Reserved.
//

package main

import "fmt"

// Problem 1
func sum(xs []int) int {
	xSum := 0
	for _, v := range xs {
		xSum += v
	}
	return xSum
}

//Problem 2
func half(x int) (int, bool) {
	return x / 2, x % 2 == 0
}

//Problem 3
func greatest(xs ...int) int {
	greatNum := xs[0]
	for _, v := range xs {
		if ( greatNum < v) {
			greatNum = v
		}
	}
	return greatNum
}

//Problem 4
func makeOddGenerator() func() uint {
	i := uint(1)
	return func() (ret uint) {
		ret = i
		i += 2
		return
	}
}

//Problem 5
func fib(n int)  int {
	// nth element in the Fibonacci sequence
	switch n {
	case 0:	return 0
	case 1: return 1
	default:
		return fib(n - 1) + fib(n - 2)
	}
}

func main() {
	xs := []int{1, 2, 3}
	fmt.Println(sum(xs))

	fmt.Println(half(1))
	fmt.Println(half(2))

	fmt.Println(greatest(3, 1, 4))
	fmt.Println(greatest(0))

	nextOdd := makeOddGenerator()
	fmt.Println(nextOdd()) // 1
	fmt.Println(nextOdd()) // 3
	fmt.Println(nextOdd()) // 5

	fmt.Println(fib(0)) // 0
	fmt.Println(fib(1)) // 1
	fmt.Println(fib(2)) // 1
	fmt.Println(fib(3)) // 2
	fmt.Println(fib(4)) // 3
	fmt.Println(fib(5)) // 5
	fmt.Println(fib(6)) // 8
}
