package main

import (
    "fmt"
    "reflect"
    "strconv"
)

func main() {
    a0 := 297547886
    ip1 := a0 >> 24
    ip2 := (a0 >> 16) & 0xFF
    ip3 := (a0 >> 8) & 0xFF
    ip4 := a0 & 0xFF
    fmt.Println(ip1)
    fmt.Println(ip2)
    fmt.Println(ip3)
    fmt.Println(ip4)
    fmt.Println(strconv.Itoa(ip1) + "." + strconv.Itoa(ip2))

    ip1 = 255
    ip2 = 255
    ip3 = 255
    ip4 = 255
    a1 := ip1 << 24 + ip2 << 16 + ip3 << 8 + ip4
    fmt.Println(a1)
    fmt.Println(reflect.TypeOf(a1))
}
