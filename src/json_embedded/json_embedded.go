package main

import (
    "encoding/json"
    "os"
)

type a struct {
    Num int `json:"num"`
}

type b struct {
    Name string `json:"name"`
    a
}

func main() {
    msg := b{
        a:a{ 1},
        Name: "Prakhar",
    }
    byteB, _ := json.Marshal(msg)
    os.Stdout.Write(byteB)
}
