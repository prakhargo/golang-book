package main

import (
    "fmt"
    "time"
    "reflect"
)

func main() {
    fmt.Println(time.Now().UnixNano() / int64(time.Millisecond))
    fmt.Println(reflect.TypeOf(time.Now().Unix()))
}
