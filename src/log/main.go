package main

import (
    "os"

    "github.com/op/go-logging"
    "fmt"
)

var log = logging.MustGetLogger("example")

// Example format string. Everything except the message has a custom color
// which is dependent on the log level. Many fields have a custom output
// formatting too, eg. the time returns the hour down to the milli second.
var format = logging.MustStringFormatter(
    `%{time:2006-01-02 15:04:05.000 Z07:00} %{level} %{shortfile} %{message}`,
)

// Password is just an example type implementing the Redactor interface. Any
// time this is logged, the Redacted() function will be called.
type Password string

func (p Password) Redacted() interface{} {
    return logging.Redact(string(p))
}

func main() {

    fileErr, err := os.OpenFile("file-error.txt", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
    if err != nil {
        fmt.Println("Failed to open log file", ":", err)
    }

    fileOut, err := os.OpenFile("file-out.txt", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
    if err != nil {
        fmt.Println("Failed to open log file", ":", err)
    }

    // For demo purposes, create two backend for os.Stderr.
    backend1 := logging.NewLogBackend(fileOut, "", 0)
    backend2 := logging.NewLogBackend(fileErr, "", 0)

    // For messages written to backend2 we want to add some additional
    // information to the output, including the used log level and the name of
    // the function.
    backend2Formatter := logging.NewBackendFormatter(backend2, format)

    // Only errors and more severe messages should be sent to backend1
    backend1Leveled := logging.AddModuleLevel(logging.NewBackendFormatter(backend1, format))
    backend1Leveled.SetLevel(logging.ERROR, "")

    // Set the backends to be used.
    logging.SetBackend(backend1Leveled, backend2Formatter)

    log.Debugf("debug %s", Password("secret"))
    log.Info("info")
    log.Notice("notice")
    log.Warning("warning")
    log.Error("err")
    log.Critical("crit")
    log.Info("Hello")
}
