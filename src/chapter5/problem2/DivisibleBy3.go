//
// Copyright (c) 2016 by Prakhar Goyal. All Rights Reserved.
//

package main

import "fmt"

func main() {
	for i := 1; i<=100; i++ {
		if (i % 3 == 0) {
			fmt.Println(i)
		}
	}
}
