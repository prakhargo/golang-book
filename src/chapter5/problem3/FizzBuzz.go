//
// Copyright (c) 2016 by Prakhar Goyal. All Rights Reserved.
//

package main

import "fmt"

func main() {
	for i := 1; i <= 100; i++ {
		fizzBuzz := 0
		if (i % 3 == 0) {
			fizzBuzz += 1
		}
		if (i % 5 == 0) {
			fizzBuzz += 2
		}
		switch fizzBuzz {
		case 0: fmt.Println(i)
		case 1: fmt.Println("Fizz")
		case 2: fmt.Println("Buzz")
		case 3: fmt.Println("FizzBuzz")
		}
	}
}
