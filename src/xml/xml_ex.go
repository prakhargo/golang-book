package main

import (
    "encoding/xml"
    "fmt"
)

type Email struct {
    Where string `xml:"where,attr"`
    Addr  string
}
type Address struct {
    //XMLName xml.Name `xml:"address"`
    City, State string
}
type Result struct {
    XMLName xml.Name `xml:"Person"`
    Name    string   `xml:"FullName"`
    Phone   string
    Email   []Email  `xml:"email"`
    Groups  []string `xml:"Group>Value"`
    Address Address  `xml:"address"`
}

func main() {
    v := Result{Name: "none", Phone: "none"}

    data := `
		<Person>
			<FullName>Grace R. Emlin</FullName>
			<Company>Example Inc.</Company>
			<email where="home">
				<Addr>gre@example.com</Addr>
			</email>
			<email where='work'>
				<Addr>gre@work.com</Addr>
			</email>
			<Group>
				<Value>Friends</Value>
				<Value>Squash</Value>
			</Group>
			<address>
                <City>Hanga Roa</City>
                <State>Easter Island</State>
			</address>
		</Person>
	`
    err := xml.Unmarshal([]byte(data), &v)
    if err != nil {
        fmt.Printf("error: %v", err)
        return
    }
    fmt.Printf("XMLName: %#v\n", v.XMLName)
    fmt.Printf("Name: %q\n", v.Name)
    fmt.Printf("Phone: %q\n", v.Phone)
    fmt.Printf("Email: %v\n", v.Email)
    fmt.Printf("Groups: %v\n", v.Groups)
    fmt.Printf("Address: %v\n", v.Address.City)
    fmt.Println(v)
}
