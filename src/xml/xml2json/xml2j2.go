package main

import (
    "encoding/xml"
    "fmt"
    "encoding/json"
)

type Result struct {
    XMLName xml.Name `xml:"message" json:"-"`
    Hello Hello `xml:"hello"`
}

type Hello struct {
    H string `xml:"h,attr" json:"h"`
    Val string  `xml:",string" json:"hello"`
}

func main() {
    v := Result{}
    data := `
            <message>
                <hello h="i">world</hello>
            </message>`
    err := xml.Unmarshal([]byte(data), &v)
    if err != nil {
        fmt.Printf("error: %v", err)
        return
    }
    fmt.Println("hi")
    fmt.Println(v)

    xjson, err := json.Marshal(v)
    fmt.Printf("%s\n",xjson)
}
