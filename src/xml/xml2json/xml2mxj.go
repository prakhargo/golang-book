package main

import (
    "encoding/xml"
    "fmt"
    "encoding/json"
    "github.com/clbanning/mxj"
)

type Result struct {
    XMLName xml.Name `xml:"message"`
    Hello Hello `xml:"hello"`
}

type Hello struct {
    H string `xml:"h,attr"`
    Val string  `xml:",string"`
}

func main() {
    v := Result{}
    data := `
            <message>
                <hello h="i">world</hello>
            </message>`
    err := xml.Unmarshal([]byte(data), &v)
    if err != nil {
        fmt.Printf("error: %v", err)
        return
    }
    fmt.Println("hi")
    fmt.Println(v)

    xjson, err := mxj.(v)
    fmt.Printf("%s\n",xjson)
}
