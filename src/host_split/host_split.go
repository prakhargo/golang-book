package main

import (
    "net"
    "strings"
    "strconv"
    "fmt"
)

func main() {
    hostList := "lancope.cisco.com"
    defaultPort := strconv.Itoa(443)
    var host, port string
    var err error
    host, port, err = net.SplitHostPort(hostList)
    if err != nil {
        fmt.Println("Inside error")
        hostPort := strings.Split(hostList, ":")
        if len(hostPort) >= 2 {
            fmt.Println("Inside if")
            port = hostPort[len(hostPort) - 1]
        }
        if port == strconv.Itoa(0) || port == "" {
            port = defaultPort
        }
        host = strings.Join(hostPort, ":")
    }
    fmt.Println(host + ":" + port)
}
