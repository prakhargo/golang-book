//
// Copyright (c) 2016 by Prakhar Goyal. All Rights Reserved.
//

package main

import "fmt"

//Problem 4
func square(x *float64) {
	*x = *x * *x
}

//Problem 5
func swap(x *int, y *int) {
	swapNum := new(int)
	*swapNum = *x
	*x = *y
	*y = *swapNum
}

func main() {
	x := 1.5
	square(&x)
	fmt.Println(x)

	a := 1
	b := 2
	swap(&a, &b)
	fmt.Println(a, b)
}
