package main

import ("fmt"; "math")

// Embedded Types and Relationships
type Person struct {
	Name string
}

type Android struct {
	Person
	Model string
}

// Methods and Interfaces
type Circle struct {
	x, y, r float64
}

func (c *Circle) area() float64 {
	return math.Pi * c.r*c.r
}

//Problem 3
func (c *Circle) perimeter() float64 {
	return 2 * math.Pi * c.r
}

type Rectangle struct {
	x1, y1, x2, y2 float64
}

func (r *Rectangle) area() float64 {
	l := distance(r.x1, r.y1, r.x1, r.y2)
	w := distance(r.x1, r.y1, r.x2, r.y1)
	return l * w
}

func distance(x1, y1, x2, y2 float64) float64 {
	a := x2 - x1
	b := y2 - y1
	return math.Sqrt(a*a + b*b)
}

//Problem 3
func (r *Rectangle) perimeter() float64 {
	l := distance(r.x1, r.y1, r.x1, r.y2)
	w := distance(r.x1, r.y1, r.x2, r.y1)
	return 2 * (l + w)
}


type Shape interface {
	area() float64
	//Problem 3
	perimeter() float64
}

func totalArea(shapes ...Shape) float64 {
	var area float64
	for _, s := range shapes {
		area += s.area()
	}
	return area
}

// Interfaces as fields
type MultiShape struct {
	shapes []Shape
}

func (m *MultiShape) area() float64 {
	var area float64
	for _, s := range m.shapes {
		area += s.area()
	}
	return area
}

func main() {
	a := new(Android)
	a.Name = "Juan"
	fmt.Println(a.Name)

	r := Rectangle{0, 0, 10, 10}
	fmt.Println(r.area())
	c := Circle{0, 0, 5}
	fmt.Println(c.area())
	fmt.Println(totalArea(&c, &r))

	//Problem 3
	fmt.Println(r.perimeter())
	fmt.Println(c.perimeter())
}
