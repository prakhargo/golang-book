//
// Copyright (c) 2016 by Prakhar Goyal. All Rights Reserved.
//

package math

// Finds the average of a series of numbers
func Average(xs []float64) float64 {
	total := float64(0)
	for _, x := range xs {
		total += x
	}
	return total / float64(len(xs))
}

//Problem 4

//Find minimum in a slice of float64s
func Min(xs []float64) float64 {
	min := xs[0]
	for _, x := range xs {
		if min > x {
			min = x
		}
	}
	return min
}

//Problem 4

//Find maximum in a slice of float64s
func Max(xs []float64) float64 {
	max := xs[0]
	for _, x := range xs {
		if max < x {
			max = x
		}
	}
	return max
}
