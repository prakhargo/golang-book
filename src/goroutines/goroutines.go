package main

import (
    "fmt"
    "sync"
)

type Number struct {
    num int
}

func NewNum(num int) *Number {
    number := new(Number)
    number.num = num
    return number
}

func (number *Number) addNum()  {
    number.num += 1
}

func (number *Number) printNum(wg *sync.WaitGroup) {
    for {
        fmt.Println("Number =", number.num)
    }
    wg.Done()
}

func main() {
    var wg sync.WaitGroup
    numInt := []int{1,2,3,4,5}
    for i := range numInt {
        num := NewNum(numInt[i])
        num.addNum()
        wg.Add(1)
        go num.printNum(&wg)
    }
    wg.Wait()
}
