package main

import (
    "net/http"
    "bytes"
    "fmt"
    //"reflect"
    "net"
    "os"
)

func check(err error) {
    if err != nil {
        panic(err)
    }
}

func main() {

    addrs, err := net.InterfaceAddrs()
    if err != nil {
        os.Stderr.WriteString("Oops: " + err.Error() + "\n")
        os.Exit(1)
    }

    for _, a := range addrs {
        if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
            if ipnet.IP.To4() != nil {
                os.Stdout.WriteString(ipnet.IP.String() + "\n")
            }
        }
    }

    soapUrl := "http://www.webservicex.net/globalweather.asmx"
    soapRequestContent := `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GetWeather xmlns="http://www.webserviceX.NET">
      <CityName>Jaipur</CityName>
      <CountryName>India</CountryName>
    </GetWeather>
  </soap:Body>
</soap:Envelope>`

    //httpClient := new(http.Client)
    soapRequest, err := http.NewRequest("POST", soapUrl, bytes.NewBufferString(soapRequestContent))

    fmt.Println("req:", soapRequest.URL.Host)
    ips, err :=net.LookupHost(soapRequest.URL.Host)
    for _, ip := range ips {
        fmt.Println(ip)
    }

    //resp, err := httpClient.Do(soapRequest)

    if err != nil {
        fmt.Println("err", err)
        return
    }
    //fmt.Println("resp:", resp)
    //fmt.Println(reflect.TypeOf(bytes.NewBufferString(soapRequest)))
    //fmt.Println(reflect.TypeOf(bytes.NewBuffer(resp.Body)))
}
