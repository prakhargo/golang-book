package main

import (
    "net/http"
    "bytes"
    "fmt"
    "reflect"
)

func main() {
    soapUrl := "http://www.webservicex.net/globalweather.asmx"
    soapRequest := `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GetWeather xmlns="http://www.webserviceX.NET">
      <CityName>Jaipur</CityName>
      <CountryName>India</CountryName>
    </GetWeather>
  </soap:Body>
</soap:Envelope>`
    httpClient := new(http.Client)
    resp, err := httpClient.Post(soapUrl, "text/xml; charset=utf-8", bytes.NewBufferString(soapRequest))

    if err != nil {
        fmt.Println("err", err)
        return
    }
    fmt.Println("resp:", resp)
    fmt.Println(reflect.TypeOf(bytes.NewBufferString(soapRequest)))
    //fmt.Println(reflect.TypeOf(bytes.NewBuffer(resp.Body)))
}
