package main

import (
    "net"
    "fmt"
)

func main() {
    ifaces, _ := net.Interfaces()
    for _, i := range ifaces {
        addrs, _ := i.Addrs()
        for _, addr := range addrs {
            var ip net.IP
            switch v := addr.(type) {
            case *net.IPNet:
                ip = v.IP
            case *net.IPAddr:
                ip = v.IP
            }
            // process IP address
            fmt.Println(ip)
            if !ip.IsLoopback() {
                fmt.Println("Non-loopback:", ip)
            }
        }
    }
}
